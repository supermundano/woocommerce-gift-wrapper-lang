��          �   %   �      `     a     x     �  
   �     �  .   �  <   �     $     0     H     X     _  f   r     �  !   �  �     K   �          "     %     *     8     =  A   F  :   �     �  �  �  !   H     j     �     �     �  9   �  J   	     O	  $   a	     �	     �	     �	  �   �	     7
  )   N
  �   x
  k   D     �     �     �     �     �     �  K   �  :   A     |                                                                                                   
   	                      Add Gift Wrap Message: Add Gift Wrap to Order Add gift wrap? After cart After checkout Allow more than one gift wrap product in cart? Are you sure you want to replace the gift wrap in your cart? Before cart Before cart collaterals Before checkout Cancel Cheatin&#8217; uh? Choose where to show gift wrap options to the customer on the cart page. You may choose more than one. Define a Category Gift wrap was added to your cart. How many characters your customer can type when creating their own note for giftwrapping. Defaults to 1000 characters; lower this number if you want shorter notes from your customers. If yes, customers can buy more than one gift wrapping product in one order. Link thumbnails? No None None selected Note Settings Should gift wrap product thumbnail images be visible in the cart? Should thumbnail images link to gift wrap product details? Yes Project-Id-Version: Woocommerce Gift Wrapper
PO-Revision-Date: 2023-06-07 16:11+0200
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.3.1
X-Poedit-Basepath: ..
X-Poedit-WPHeader: woocommerce-gift-wrapper.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Añadir mensaje a la caja regalo: Añadir caja regalo al pedido ¿Añadir caja regalo? Después del carrito Después del checkout ¿Permitir más de un producto de caja regalo en carrito? ¿Está usted seguro de que desea reemplazar la caja regalo en tu carrito? Antes del carrito Antes de los colaterales del carrito Antes del checkout Cancelar Cheatin&#8217; uh? Elegir dónde Mostrar opciones de envoltorio de regalo para el cliente en la página del carrito. Usted puede elegir más de uno. Definir una categoría Envoltorio de regalo añadido al carrito. Cuántos caracteres su cliente puede escribir al crear su propia Nota para giftwrapping. Valores por defecto a 1000 caracteres; bajar este número si usted quiere más breves comentarios de sus clientes. Si es así, los clientes pueden comprar más de una caja regalo envolviendo el producto en un mismo pedido. Enlazar thumbnails? No Ninguno Ninguno seleccionado Nota Parámetros ¿Deben las imágenes miniatura de cajas regalo ser visibles en el carrito? Should thumbnail images link to gift wrap product details? Sí 